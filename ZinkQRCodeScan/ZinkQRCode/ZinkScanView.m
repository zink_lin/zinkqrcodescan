//
//  ZinkScanView.m
//  ZinkQRCodeScan
//
//  Created by Zink on 14-9-9.
//  Copyright (c) 2014年 林峥. All rights reserved.
//

#import "ZinkScanView.h"

#define Label_Width     65
#define Label_Height    20

#define persentYAll     100
#define persentYMin     24
#define persentYMax     74

#define persentXAll     100
#define persentXMin     21
#define persentXMax     79

@implementation ZinkScanView{
    UIImageView * _scanLine;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(void)prepareToScan{
    self.backgroundColor = [UIColor darkGrayColor];
    UILabel * label = [[UILabel alloc] initWithFrame:CGRectMake(self.frame.size.width/2-Label_Width/2, self.frame.size.height/2, Label_Width, Label_Height)];
    [label setBackgroundColor:[UIColor clearColor]];
    [label setTextColor:[UIColor whiteColor]];
    [label setText:@"加载中"];
    [self addSubview:label];
    UIActivityIndicatorView * activityView = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(self.frame.size.width/2+Label_Width/2, self.frame.size.height/2+Label_Height/2, 0, 0)];
    [activityView startAnimating];
    
    [self addSubview:activityView];
    
    UIImageView * imageBG = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
    [imageBG setImage:[UIImage imageNamed:@"scanBG.png"]];
    [self addSubview:imageBG];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        if (ZIOSVersion>=7.0) {
            AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
            if (authStatus == AVAuthorizationStatusRestricted || authStatus ==AVAuthorizationStatusDenied)
            {
                UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"当前相机不可用" message:@"请在系统设置中(设置->隐私->相机)开启相机使用权限" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles: nil];
                [alert show];
            }else{
                [self startToScan];
            }
        }else{
            [self startToScan];
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            if (ZIOSVersion>=7.0) {
                [self.layer insertSublayer:_preview atIndex:0];
            
                [_session startRunning];
            }else{
                [self insertSubview:_viewReader atIndex:0];
                
                [_viewReader start];
            }
            
            [label removeFromSuperview];
            [activityView stopAnimating];
            [activityView removeFromSuperview];
            
            _scanLine = [[UIImageView alloc] initWithFrame:CGRectMake(self.frame.size.width*persentXMin/persentXAll, self.frame.size.height*persentYMin/persentYAll, self.frame.size.width*(persentXMax-persentXMin)/persentXAll, 2)];
            [_scanLine setImage:[UIImage imageNamed:@"scanLine.png"]];
            [self addSubview:_scanLine];
            
            [self startLineAnimationWith:_scanLine.frame];
        });
    });
    
    
}

-(void)startLineAnimationWith:(CGRect)lineFrame{
    [UIView animateWithDuration:3.0f animations:^{
        [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
        [UIView setAnimationRepeatCount:HUGE_VALF];
        [UIView setAnimationRepeatAutoreverses:YES];
        [_scanLine setFrame:CGRectMake(lineFrame.origin.x,
                                       self.frame.size.height*persentYMax/persentYAll,
                                       lineFrame.size.width,
                                       lineFrame.size.height)];
    } completion:^(BOOL finished) {
        [_scanLine setFrame:CGRectMake(lineFrame.origin.x,
                                       self.frame.size.height*persentYMin/persentYAll,
                                       lineFrame.size.width,
                                       lineFrame.size.height)];
    }];
}

-(void)stopToScan{
    if (ZIOSVersion>=7.0) {
        _input = nil;
        _device = nil;
        _output = nil;
        _stillImageOutput = nil;
        [_session stopRunning];
        _session = nil;
        [_preview removeFromSuperlayer];
        _preview = nil;
    }else{
        [_viewReader stop];
        [_viewReader removeFromSuperview];
        _viewReader = nil;
    }
}

-(void)startToScan{
    _isScaning = YES;
    if (ZIOSVersion >= 7.0) {
        if (_session) {
            [_session startRunning];
        }else{
            [self setUpCameraBySystem];
        }
    }else{
        if (_viewReader) {
            [_viewReader start];
        }else{
            [self setUpCameraByZbar];
        }
    }
}

- (void)setUpCameraBySystem
{
    // Device
    _device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    // Input
    _input = [AVCaptureDeviceInput deviceInputWithDevice:_device error:nil];
    // Output
    _output = [[AVCaptureMetadataOutput alloc]init];
    [_output setMetadataObjectsDelegate:self queue:dispatch_get_main_queue()];
    ////////输出图像的ouput
    _stillImageOutput = [[AVCaptureStillImageOutput alloc] init];
    NSDictionary *outputSettings = [[NSDictionary alloc] initWithObjectsAndKeys:AVVideoCodecJPEG,AVVideoCodecKey,nil];
    [_stillImageOutput setOutputSettings:outputSettings];

    // Session
    _session = [[AVCaptureSession alloc]init];
    [_session setSessionPreset:AVCaptureSessionPresetHigh];
    
    
    if ([_session canAddInput:_input])
    {
        [_session addInput:_input];
    }
    [_session addOutput:_stillImageOutput];
    if ([_session canAddOutput:_output])
    {
        [_session addOutput:_output];
    }
    
    
    // 条码类型 AVMetadataObjectTypeQRCode
    _output.metadataObjectTypes =@[AVMetadataObjectTypeQRCode];
    
    // Preview
    _preview =[AVCaptureVideoPreviewLayer layerWithSession:_session];
    _preview.videoGravity = AVLayerVideoGravityResizeAspectFill;
    _preview.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
}

-(void)setUpCameraByZbar{
    _viewReader = [[ZBarReaderView alloc]init];
    _viewReader.readerDelegate = self;
    _viewReader.torchMode = 0;
    _viewReader.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
}

#pragma mark AVCaptureMetadataOutputObjectsDelegate
- (void)captureOutput:(AVCaptureOutput *)captureOutput didOutputMetadataObjects:(NSArray *)metadataObjects fromConnection:(AVCaptureConnection *)connection
{
    
    //获取码体内容
    NSString *stringValue;
    if ([metadataObjects count] >0 && _isScaning)
    {
        _isScaning = NO;
        AVMetadataMachineReadableCodeObject * metadataObject = [metadataObjects objectAtIndex:0];
        stringValue = metadataObject.stringValue;
        
        
        
        //播放音效
        SystemSoundID mBeep;
        NSString* path = [[NSBundle mainBundle]
                          pathForResource:@"beep-beep" ofType:@"aiff"];
        NSURL* url = [NSURL fileURLWithPath:path];
        AudioServicesCreateSystemSoundID((__bridge CFURLRef)url, &mBeep);
        AudioServicesPlaySystemSound(mBeep);
        

        AVCaptureConnection *videoConnection = nil;
        for (AVCaptureConnection *connection in _stillImageOutput.connections) {
            for (AVCaptureInputPort *port in [connection inputPorts]) {
                if ([[port mediaType] isEqual:AVMediaTypeVideo]) {
                    videoConnection = connection;
                    break;
                }
            }
            if (videoConnection) {
                break;
            }
        }
        
        // block方法 & 记得 #import <ImageIO/ImageIO.h>
        [_stillImageOutput captureStillImageAsynchronouslyFromConnection:videoConnection completionHandler:
         ^(CMSampleBufferRef imageSampleBuff, NSError *error){
             NSData *imageData = [AVCaptureStillImageOutput jpegStillImageNSDataRepresentation:imageSampleBuff];
             
             UIImage *image = [UIImage imageWithData:imageData];
             
             [_session stopRunning];
             
             
             [self.delegate zinkScanViewDidReadQRCode:self withCodeContent:stringValue andCodeImage:image];
         }];
    }
    
}


#pragma zbarDelegate
-(void)readerView:(ZBarReaderView *)readerView didReadSymbols:(ZBarSymbolSet *)symbols fromImage:(UIImage *)image{
    
    NSString *stringValue;
    for (ZBarSymbol *symbol in symbols) {
        stringValue = symbol.data;
        break;
    }
    
    [_viewReader stop];
    //播放音效
    SystemSoundID mBeep;
    NSString* path = [[NSBundle mainBundle]
                      pathForResource:@"beep-beep" ofType:@"aiff"];
    NSURL* url = [NSURL fileURLWithPath:path];
    AudioServicesCreateSystemSoundID((__bridge CFURLRef)url, &mBeep);
    AudioServicesPlaySystemSound(mBeep);
    
    [self.delegate zinkScanViewDidReadQRCode:self withCodeContent:stringValue andCodeImage:image];

}

@end

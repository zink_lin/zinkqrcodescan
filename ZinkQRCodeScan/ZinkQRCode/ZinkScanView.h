//
//  ZinkScanView.h
//  ZinkQRCodeScan
//
//  Created by Zink on 14-9-9.
//  Copyright (c) 2014年 林峥. All rights reserved.
//

/**
 导入AVFoundation.framwork, CoreMedia.framework, CoreVideo.framework, libiconv.dylib
 
 
 */

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <ImageIO/ImageIO.h>
#import "ZBarSDK.h"
#import "ZinkGlobal.h"

@protocol ZinkScanViewDelegate;


@interface ZinkScanView : UIView<AVCaptureMetadataOutputObjectsDelegate, AVCaptureVideoDataOutputSampleBufferDelegate, ZBarReaderViewDelegate>{
    ZBarReaderView * _viewReader;
    
    AVCaptureDevice * _device;
    AVCaptureDeviceInput * _input;
    AVCaptureMetadataOutput * _output;
    AVCaptureStillImageOutput * _stillImageOutput;
    
    AVCaptureSession * _session;
    AVCaptureVideoPreviewLayer * _preview;

}



@property(strong, nonatomic)id<ZinkScanViewDelegate> delegate;
@property(assign, nonatomic)BOOL isScaning;

//这个方法在扫描准备完成之后，会自动调用startToScan进行扫描
-(void)prepareToScan;
-(void)stopToScan;
-(void)startToScan;

@end


@protocol ZinkScanViewDelegate <NSObject>

//成功扫描到二维码后，会停止继续扫码，需要调用startCamera重新开启扫描
-(void)zinkScanViewDidReadQRCode:(ZinkScanView *)view withCodeContent:(NSString *)content andCodeImage:(UIImage *)image;

@end
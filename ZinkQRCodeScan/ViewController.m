//
//  ViewController.m
//  ZinkQRCodeScan
//
//  Created by Zink on 14-9-9.
//  Copyright (c) 2014年 林峥. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    ZinkScanView * zinkScanView = [[ZinkScanView alloc] initWithFrame:CGRectMake(0, 0, 200, 200)];
    [zinkScanView prepareToScan];
    zinkScanView.delegate = self;
    
    [self.view addSubview:zinkScanView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark zinkScan
-(void)zinkScanViewDidReadQRCode:(ZinkScanView *)view withCodeContent:(NSString *)content andCodeImage:(UIImage *)image{
    
    [view startToScan];
    Zdebug(@"%@, %@",content, image);
    
    [self.imageView setImage:image];
}

@end

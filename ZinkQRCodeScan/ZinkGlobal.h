//
//  ZinkGlobal.h
//
//  Created by mac on 14-4-1.
//  Copyright (c) 2014年 林峥. All rights reserved.
//


// debug log
#define ZDebugLog 0
#ifdef  ZDebugLog
#define Zdebug(...) NSLog(__VA_ARGS__)
#else
#define Zdebug(...) {};
#endif

// property
#define ZTabBarHeight                        49.0f
#define ZNaviBarHeight                       44.0f
#define ZStatusBarHeight                     [UIApplication sharedApplication].statusBarFrame.size.height
#define ZContentHeight                       ZScreenHeight-ZTabBarHeight-ZNaviBarHeight-ZStatusBarHeight


#define ZHeightFor4InchScreen                568.0f
#define ZHeightFor3p5InchScreen              480.0f

// method
#define ZIOSVersion                          [[[UIDevice currentDevice] systemVersion] floatValue]
#define ZIsiOS7Later                         !(IOSVersion < 7.0)

#define ZScreenRect                          [[UIScreen mainScreen] bounds]
#define ZScreenWidth                         [[UIScreen mainScreen] bounds].size.width
#define ZScreenHeight                        [[UIScreen mainScreen] bounds].size.height

//private
#define ZNavTitle_Width                      150.0f
#define ZNavTitle_Height                     25.0f

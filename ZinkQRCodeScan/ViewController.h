//
//  ViewController.h
//  ZinkQRCodeScan
//
//  Created by Zink on 14-9-9.
//  Copyright (c) 2014年 林峥. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZinkScanView.h"

@interface ViewController : UIViewController<ZinkScanViewDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@end
